# Internet Artifice

Internet Artifice is to showcase application including a number of features.

1. Agile Project boards(complete, w/ auth-blocked write features)
2. Time tracker(v0.4.0)
3. Abstract note keeping(future)

The primary technologies are Angular 11 running on the PaaS, Firebase.

## Development server

This can be done in a number of ways, but to avoid CORS issues. Run the following.

Run in terminal one: `cd functions/; npm run serve` for a dev cloud functions server. Restart this when making function changes.
Run in terminal two: `npm run start` for a dev frontend server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `public/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Trigger cloud functions locally

Run the firebase emulator for the cloud functions.

```bash
cd ~/<project-dir>/functions && npm run serve
```

Once that is running, use the following curl requests to play around.
