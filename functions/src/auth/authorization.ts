// eslint-disable-next-line
import { Request, Response } from 'firebase-functions';
// eslint-disable-next-line
import { Project } from '../../../src/app/models/data/project';

/**
 * Checks business-level permissions
 * @param {Request} request
 * @param {Response} response
 * @param {Array} opts
 * @return {Promise<boolean>}
 */
async function isAuthorized(
  request: Request,
  response: Response,
  opts: { hasRole: Array<'admin'>; allowSameUser?: boolean }
): Promise<boolean> {
  const { role } = response.locals;

  if (request !== null && !role) {
    response.status(403).json({ error: 'You no roles assigned' });
    return false;
  }

  if (opts.hasRole.includes(role)) {
    return true;
  }

  response
    .status(403)
    .json({ error: 'You do not have the role to perform this action' });
  return false;
}

/**
 * Validate project and user exists and are permissible
 * @todo Implement project permissions
 * @param {Project} project
 * @return {boolean}
 */
function isProjectValid(project: Project) {
  return project.name !== '';
}

/**
 * Validate project Id
 * @param {string} projectId
 * @return {boolean}
 */
function isProjectIdValid(projectId: string) {
  return projectId !== '';
}

/**
 * Basic scrum item validation for database inserts
 * @todo Update functions to validate users, and further into data structure
 * @param {Object} object from incoming request
 * @return {Boolean} returns true if valid data provided, otherwise false
 */
function isValidNewScrumItem(object: Object): Boolean {
  /* eslint no-prototype-builtins: "error"*/
  return ['title', 'description', 'progress', 'status'].every(key =>
    Object.prototype.hasOwnProperty.call(object, key)
  );
}

/**
 * Basic scrum item validation function for database updates
 * @todo Update functions to validate users, and further into data structure
 * @param {Object} object from incoming request
 * @return {Boolean} returns true if valid data provided, otherwise false
 */
function isValidUpdateScrumItem(object: Object): Boolean {
  /* eslint no-prototype-builtins: "error"*/
  return (
    isValidNewScrumItem(object) &&
    Object.prototype.hasOwnProperty.call(object, 'id')
  );
}

export {
  isAuthorized,
  isProjectValid,
  isProjectIdValid,
  isValidNewScrumItem,
  isValidUpdateScrumItem,
};
