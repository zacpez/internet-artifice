import * as admin from 'firebase-admin';
import { addItem, updateItem, getItems, deleteItem }  from './services/projects/items';
import { getProjectInfo, getProjects, putProject } from './services/projects';
import { validateOneTimeCode } from './services/user';

admin.initializeApp();

export const api = {
  items: {
    getItems,
    addItem,
    updateItem,
    deleteItem,
  },
  projects: {
    getProjectInfo,
    getProjects,
    putProject,
  },
  user: {
    validateOneTimeCode,
  },
};
