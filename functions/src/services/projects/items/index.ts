import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { SCRUM_COLLECTION, SCRUM_ITEM_COLLECTION } from '../../../constants';
import { isAuthenticated, modifyResponse } from '../../../auth/authentication';
import {
  isAuthorized,
  isProjectIdValid,
  isValidNewScrumItem,
  isValidUpdateScrumItem,
} from '../../../auth/authorization';
import { ScrumItem } from '../../../models/scrum-item';

/**
 * Basic cloud functions for Project item CRUD
 * @todo add support for GET to query/search on conditions
 * @todo add support for GET to limit resulting items
 */

/**
 * Add new Project item to an appropriate collection
 * @returns {Object} response
 * @returns {string} response.itemId for a successfully create Project item
 * @returns {string} response.message for erroneous request,
 *                   action not done
 */
 export const addItem = functions.https.onRequest(async (request, response) => {
  if (request.method === 'OPTIONS') {
    response = modifyResponse(request, response);
    response.send();
    return;
  }
  if (!(await isAuthenticated(request, response))) {
    return;
  }
  if (!(await isAuthorized(request, response, { hasRole: ['admin'] }))) {
    return;
  }
  if (!request.body.item) {
    response = modifyResponse(request, response, 400);
    response.json({ message: 'Project item not supplied' });
    return;
  }
  if (isValidNewScrumItem(request.body.item)) {
    const writeResult = await admin
      .firestore()
      .collection(SCRUM_COLLECTION)
      .doc(request.body.project)
      .collection(SCRUM_ITEM_COLLECTION)
      .add(request.body.item);

    response = modifyResponse(request, response);
    response.json({ itemId: writeResult.id });
    return;
  }

  response = modifyResponse(request, response, 400);
  response.json({ message: 'Did not insert the Project item.' });
});

/**
 * Update a Project item in the appropriate collection
 * @return {Object} response
 * @return {Array} response.items for a successfully retrieved Project items
 * @return {string} response.message for erroneous request,
 *                  action not done
 */
 export const updateItem = functions.https.onRequest(async (request, response) => {
  if (request.method === 'OPTIONS') {
    response = modifyResponse(request, response);
    response.send();
    return;
  }
  if (!(await isAuthenticated(request, response))) {
    return;
  }
  if (!(await isAuthorized(request, response, { hasRole: ['admin'] }))) {
    return;
  }
  if (!request.body.item) {
    response = modifyResponse(request, response, 404);
    response.json({ message: 'Project item not supplied' });
    return;
  }
  if (isValidUpdateScrumItem(request.body.item)) {
    await admin
      .firestore()
      .collection(SCRUM_COLLECTION)
      .doc(request.body.project)
      .collection(SCRUM_ITEM_COLLECTION)
      .doc(request.body.item.id)
      .update(request.body.item);

    response = modifyResponse(request, response);
    response.json({ itemId: request.body.id });
    return;
  }

  response = modifyResponse(request, response, 400);
  response.json({ message: 'Did not update the Project item.' });
  return;
});

/**
 * Remove a Project item from the appropriate collection
 * @return {Object} response
 * @return {string} response.itemId for a successfully removed Project item
 * @return {string} response.message for erroneous request,
 *                  action not done
 */
 export const deleteItem = functions.https.onRequest(async (request, response) => {
  if (request.method === 'OPTIONS') {
    response = modifyResponse(request, response);
    response.send();
    return;
  }
  if (!(await isAuthenticated(request, response))) {
    return;
  }
  if (!(await isAuthorized(request, response, { hasRole: ['admin'] }))) {
    return;
  }
  if (!request.body.item) {
    response = modifyResponse(request, response, 400);
    response.json({ message: 'Project item not supplied' });
    return;
  }
  if (request.body.item.id) {
    await admin
      .firestore()
      .collection(SCRUM_COLLECTION)
      .doc(request.body.project)
      .collection(SCRUM_ITEM_COLLECTION)
      .doc(request.body.item.id)
      .delete();

    response = modifyResponse(request, response);
    response.json({ itemId: request.body.id });
    return;
  }

  response = modifyResponse(request, response, 400);
  response.json({ message: 'Did not remove Project item by id.' });
  return;
});

/**
 * Get a Project item to an appropriate collection
 * @return {Object} response
 * @return {string} response.itemId for a successfully create Project item
 * @return {string} response.message for erroneous request,
 *                  action not done
 */
export const getItems = functions.https.onRequest(async (request, response) => {
  let projectId: string = '';
  if (typeof request.query['project'] === 'string') {
    projectId = request.query['project'];
  }

  if (isProjectIdValid(projectId)) {
    const docSnapshot = await admin
      .firestore()
      .collection(SCRUM_COLLECTION)
      .doc(projectId);
    if ((await docSnapshot.get()).exists) {
      const querySnapshot = await docSnapshot
        .collection(SCRUM_ITEM_COLLECTION)
        .get();

      const items = querySnapshot.docs
        .map(doc => {
          return { ...doc.data(), id: doc.id } as ScrumItem;
        })
        .sort((a: ScrumItem, b: ScrumItem) => a.order - b.order);

      response = modifyResponse(request, response);
      response.json(items);
      return;
    }
  }

  response = modifyResponse(request, response, 404);
  response.json({
    message: 'Project items do not exist or you cannot view them.',
  });
  return;
});
