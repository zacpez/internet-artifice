import { https } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { SCRUM_COLLECTION } from '../../constants';
import { isProjectValid, isAuthorized } from '../../auth/authorization';
// eslint-disable-next-line
import { Project } from '../../../../src/app/models/data/project';
import { isAuthenticated, modifyResponse } from '../../auth/authentication';

const getProjectInfo = https.onRequest(async (request, response) => {
  let projectName: string = '';
  if (typeof request.query['name'] === 'string') {
    projectName = request.query['name'];
  }

  const querySnapshot = await firestore()
    .collection(SCRUM_COLLECTION)
    .doc(projectName)
    .get();

  const project: Project = {
    ...querySnapshot.data(),
    id: querySnapshot.id,
  } as Project;

  if (isProjectValid(project) && querySnapshot.exists) {
    response = modifyResponse(request, response);
    response.json(project);
    return;
  }

  response = modifyResponse(request, response, 404);
  response.json({
    message: 'Project does not exist or you cannot view it.',
  });
  return;
});

/**
 * Get a list of projects
 * @return {Object} response
 * @return {string} response.itemId for a successfully create scrum item
 * @return {string} response.error message for erroneous request,
 *                  action not done
 */
const getProjects = https.onRequest(async (request, response) => {
  const querySnapshot = await firestore().collection(SCRUM_COLLECTION).get();

  const project = querySnapshot.docs.map(doc => {
    return { ...doc.data(), id: doc.id };
  });

  response = modifyResponse(request, response);
  response.json(project);
});

const putProject = https.onRequest(async (request, response) => {
  if (request.method === 'OPTIONS') {
    response = modifyResponse(request, response);
    response.send();
    return;
  }
  if (!(await isAuthenticated(request, response))) {
    return;
  }
  if (!(await isAuthorized(request, response, { hasRole: ['admin'] }))) {
    return;
  }
  if (!request.body.project) {
    response = modifyResponse(request, response, 400);
    response.json({ message: 'Project to update not supplied' });
    return;
  }
  // id only belongs as a key, not field and value.
  const { id, ...projectData } = request.body.project;
  const querySnapshot = await firestore()
    .collection(SCRUM_COLLECTION)
    .doc(id)
    .update(projectData);

  // const projects = querySnapshot.docs.map((doc) => {
  //   return {...doc.data(), id: doc.id};
  // });

  response = modifyResponse(request, response);
  response.json({ ...querySnapshot });
});

export { getProjectInfo, getProjects, putProject };
