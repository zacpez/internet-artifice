import { https } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { modifyResponse } from '../auth/authentication';

import { ONE_TIME_CODES_COLLECTION } from '../constants';

const validateOneTimeCode = https.onRequest(async (request, response) => {
  const { email, oneTimeCode } = request.body;
  if (typeof oneTimeCode === 'string') {
    response = modifyResponse(request, response, 400);
    response.json({
      message: 'Provide a one time code to register',
    });
    return;
  }

  if (typeof email === 'string') {
    response = modifyResponse(request, response, 400);
    response.json({
      message: 'Provide an email to receive a one time code to register',
    });
    return;
  }

  const querySnapshot = await firestore()
    .collection(ONE_TIME_CODES_COLLECTION)
    .doc(oneTimeCode)
    .get();

  if (querySnapshot.exists) {
    await firestore()
      .collection(ONE_TIME_CODES_COLLECTION)
      .doc(oneTimeCode)
      .delete();
    response = modifyResponse(request, response);
    response.json({ found: true });
    return;
  }

  response = modifyResponse(request, response, 404);
  response.json({
    message: 'One time code could not be found, or is no longer valid.',
  });
  return;
});

export { validateOneTimeCode };
