const SCRUM_COLLECTION = 'scrum';
const SCRUM_INFO_COLLECTION = 'info';
const SCRUM_ITEM_COLLECTION = 'items';
const PROJECTS_COLLECTION = 'projects';
const ONE_TIME_CODES_COLLECTION = 'oneTimeCodes';

export {
  SCRUM_COLLECTION,
  SCRUM_INFO_COLLECTION,
  SCRUM_ITEM_COLLECTION,
  PROJECTS_COLLECTION,
  ONE_TIME_CODES_COLLECTION,
};
