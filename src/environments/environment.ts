// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:5001/angulardemo-301801/us-central1/',
  firebase: {
    apiKey: 'AIzaSyDL_rp9ZlGVIdihaasEbHhMXmVhlfM8xXY',
    authDomain: 'angulardemo-301801.firebaseapp.com',
    projectId: 'angulardemo-301801',
    storageBucket: 'angulardemo-301801.appspot.com',
    messagingSenderId: '761145170951',
    appId: '1:761145170951:web:3290d9c547c9bab5b35ea2',
    measurementId: 'G-HH7VJC3GVM',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
