import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {
  ActivatedRoute,
  Router,
  Event,
  NavigationStart,
} from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Internet Artifice';

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.router.events.subscribe(() => {
      const child = this.activatedRoute.firstChild ?? this.activatedRoute;
      this.router.events
        .pipe(filter(event => event instanceof NavigationStart))
        .subscribe((data: Event) => {
          this.activatedRoute.data.subscribe(data => {
            this.titleService.setTitle(data.title ?? this.title);
          });
        });
    });
  }
}
