/**
 * Basic single state model, for a toggle.
 * @var title is used for a identifier for what state is next
 * @var remainderDescription payload of this state state
 */
export class ToggleState {
  title: string = '';
  remainderDescription: string = '';
}
