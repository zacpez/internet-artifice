import { ScrumItem as BaseItem } from '../data/scrum-item';
import { Validators } from '@angular/forms';

export class ScrumItem extends BaseItem {
  public editing: boolean = false;
  constructor(rawItem: any = {}) {
    super(rawItem);
    if (rawItem) {
      this.editing = rawItem.editing;
    }
  }

  /**
   * Form validation requirements
   * @returns
   */
  static getFormGroup() {
    return {
      id: ['', Validators.required],
      title: ['', Validators.required],
      description: ['', Validators.required],
      status: ['', Validators.required],
      progress: [0, Validators.required],
      editing: [false, Validators.required],
      version: ['', Validators.required],
      order: [0, Validators.required],
      colour: ['#ffffff'],
    };
  }
}
