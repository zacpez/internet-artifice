import { BasicAuth as BaseItem } from '../data/basic-auth';
import { Validators } from '@angular/forms';

export class BasicAuth extends BaseItem {
  /**
   * Login form validation requirements
   * @returns
   */
  static getLoginFormGroup(): any {
    return {
      email: ['', Validators.required],
      password: ['', Validators.required],
    };
  }

  /**
   * Request Access form validation requirements
   * @returns
   */
  static getRequestAccessFormGroup(): void {}
}
