/**
 * Project to organize sets of work
 * @todo Add unique id system
 */
export class Project {
  public id: string = '';
  public name: string = '';
  public description: string = '';
  public tags: String[] = [];
  public createdAt: Date = new Date();
  public updatedAt: Date = new Date();
  public isPrivate: Boolean = true;

  constructor(rawItem: any = {}) {
    if (rawItem) {
      this.id = rawItem.id;
      this.name = rawItem.name;
      this.description = rawItem.description;
      this.tags = rawItem.tags;
      this.createdAt = rawItem.createdAt;
      this.updatedAt = rawItem.updatedAt;
      this.isPrivate = rawItem.isPrivate;
    }
  }
}
