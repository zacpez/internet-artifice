/**
 * Scrum Items to organize work
 * @todo Add unique id system
 */
export class ScrumItem {
  public id: string = '';
  public title: string = '';
  public description: string = '';
  public progress = 0;
  public status: string = '';
  public version: string = '';
  public colour: string = '#ffffff';
  public order: number = 0;

  constructor(rawItem: any = {}) {
    if (rawItem) {
      this.id = rawItem.id;
      this.title = rawItem.title;
      this.description = rawItem.description;
      this.progress = rawItem.progress;
      this.status = rawItem.status;
      this.version = rawItem.version;
      this.colour = rawItem.colour;
      this.order = rawItem.order || 0;
    }
  }
}
