import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NgIconsModule } from '@ng-icons/core';

import * as tabler from '@ng-icons/tabler-icons';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScrumItemComponent } from './components/molecule/scrum-item/scrum-item.component';
import { BoardPageComponent } from './components/pages/board/board.component';
import { BasicTemplateComponent } from './components/templates/basic-template/basic-template.component';
import { ScrumListComponent } from './components/organism/scrum-list/scrum-list.component';
import { HttpClientModule } from '@angular/common/http';
import { EditScrumItemComponent } from './components/molecule/edit-scrum-item/edit-scrum-item.component';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule, SETTINGS } from '@angular/fire/compat/auth';
import { SETTINGS as AUTH_SETTINGS } from '@angular/fire/compat/auth';
import { ContentLoaderModule } from '@ngneat/content-loader';
import { TextFieldModule } from '@angular/cdk/text-field';
import { environment } from '../environments/environment';
import { ModalComponent } from './components/organism/modal/modal.component';
import { LoginComponent as ModalLogin } from './components/organism/modal/login/login.component';
import { LoginComponent as FormLogin } from './components/organism/form/login/login.component';
import { RequestAccessComponent as FormRequestAccess } from './components/organism/form/request-access/request-access.component';
import { HomePageComponent } from './components/pages/home-page/home-page.component';
import { ErrorPageComponent } from './components/pages/error-page/error-page.component';
import { SearchBoardsPageComponent } from './components/pages/search-boards-page/search-boards-page.component';
import { ProjectListItemLoaderComponent } from './components/molecule/content-loader/project-list-item-loader/project-list-item-loader.component';
import { ScrumItemLoaderComponent } from './components/molecule/content-loader/scrum-item/scrum-item-loader.component';
import { ProjectHeaderLoaderComponent } from './components/molecule/content-loader/project-header-loader/project-header-loader.component';
import { TimeTrackerComponent } from './components/pages/time-tracker/time-tracker.component';
import { ErrorComponent } from './components/organism/modal/error/error.component';
import { ProjectHeaderComponent } from './components/organism/project-header/project-header.component';
import { RequestAccessComponent } from './components/pages/request-access/request-access.component';
import { BrowserModule } from '@angular/platform-browser';
import { LoginLogoutComponent } from './components/atoms/buttons/login-logout/login-logout.component';
import { AboutComponent } from './components/pages/about/about.component';
import { DocsComponent } from './components/pages/docs/docs.component';
import { ButtonComponent } from './components/atoms/button/button.component';
import { ListComponent } from './components/pages/list/list.component';

@NgModule({
  declarations: [
    AppComponent,
    ScrumItemComponent,
    ErrorPageComponent,
    HomePageComponent,
    BoardPageComponent,
    BasicTemplateComponent,
    ScrumListComponent,
    EditScrumItemComponent,
    ModalComponent,
    ModalLogin,
    FormLogin,
    FormRequestAccess,
    SearchBoardsPageComponent,
    ProjectListItemLoaderComponent,
    ScrumItemLoaderComponent,
    ProjectHeaderLoaderComponent,
    TimeTrackerComponent,
    ErrorComponent,
    ProjectHeaderComponent,
    RequestAccessComponent,
    LoginLogoutComponent,
    AboutComponent,
    DocsComponent,
    ListComponent,
    ButtonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgIconsModule.withIcons(tabler),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    ContentLoaderModule,
    DragDropModule,
    TextFieldModule,
  ],
  providers: [
    {
      provide: AUTH_SETTINGS,
      useValue: { appVerificationDisabledForTesting: true },
    },
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
