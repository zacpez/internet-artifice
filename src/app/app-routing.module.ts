import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { BoardPageComponent } from './components/pages/board/board.component';
import { HomePageComponent } from './components/pages/home-page/home-page.component';
import { ErrorPageComponent } from './components/pages/error-page/error-page.component';
import { SearchBoardsPageComponent } from './components/pages/search-boards-page/search-boards-page.component';
import { TimeTrackerComponent } from './components/pages/time-tracker/time-tracker.component';
import packageInfo from 'package.json';
import { RequestAccessComponent } from './components/pages/request-access/request-access.component';

const year: number = new Date().getFullYear();
const { version } = packageInfo;

const routeFactory = (route: Route) => {
  return { ...route, data: { year, version, ...route.data } };
};

const routes: Routes = [
  routeFactory({
    path: '',
    data: {},
    component: HomePageComponent,
  }),
  routeFactory({
    path: 'board/:projectName',
    component: BoardPageComponent,
  }),
  routeFactory({
    path: 'boards',
    data: { title: 'Search Projects' },
    component: SearchBoardsPageComponent,
  }),
  routeFactory({
    path: 'request-access',
    data: { title: 'Internet Artifice Access Request' },
    component: RequestAccessComponent,
  }),
  routeFactory({
    path: 'tracker',
    data: { title: 'Time Tracker' },
    component: TimeTrackerComponent,
  }),
  routeFactory({
    path: '**',
    data: {},
    component: ErrorPageComponent,
  }),
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
