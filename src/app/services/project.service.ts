import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { BasicAuthService } from './basic-auth.service';
import { Project } from '../models/data/project';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProjectService {
  // BLL function names
  GET_PROJECT_INFO_URL = 'api-projects-getProjectInfo';
  GET_PROJECTS_URL = 'api-projects-getProjects';
  PUT_PROJECT_URL = 'api-projects-putProject';

  // Request headers for this session
  postHeaders: { [headers: string]: HttpHeaders };
  getHeaders: { [headers: string]: HttpHeaders };

  projectName: string = '';
  loading: boolean = true;

  /**
   *
   * @param http A client used to call endpoint
   * @param basicAuthService A service to operate authentication & authorization requests
   */
  constructor(
    private http: HttpClient,
    private basicAuthService: BasicAuthService
  ) {
    this.GET_PROJECT_INFO_URL = environment.apiUrl + this.GET_PROJECT_INFO_URL;
    this.GET_PROJECTS_URL = environment.apiUrl + this.GET_PROJECTS_URL;
    this.PUT_PROJECT_URL = environment.apiUrl + this.PUT_PROJECT_URL;

    // Default headers
    const basicHeaders: any = {
      'Content-Type': 'application/json',
    };

    // Store headers for this session's requests
    this.postHeaders = {
      headers: new HttpHeaders(basicHeaders),
    };
    this.getHeaders = {
      headers: new HttpHeaders(basicHeaders),
    };
  }

  // Local copy of Project
  project: Project = new Project();
  error?: HttpErrorResponse | null;
  errorEvents: Subject<string> = new Subject<string>();

  /**
   * Get a project
   * @return {Project} selected by query params
   */
  public getProjectInfo(projectName: string): Observable<Project> {
    return this.http.get<Project>(
      this.GET_PROJECT_INFO_URL + '?name=' + projectName,
      this.getHeaders
    );
  }

  /**
   *
   * @param {string} projectName
   * @returns {Observable<Project[]>} an array of Project models
   */
  public getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.GET_PROJECTS_URL, this.getHeaders);
  }

  /**
   * Fetches and stores items in this service from a project
   */
  public loadProjectInfo(projectName: string = ''): Observable<Project> {
    this.loading = true;
    this.error = null;
    if (projectName) {
      this.projectName = projectName;
    }
    return this.getProjectInfo(this.projectName).pipe(
      tap(
        (project: Project) => {
          this.project = project;
          this.loading = false;
        },
        (error: HttpErrorResponse) => {
          this.error = error;
          this.errorEvents.next('open');
        }
      )
    );
  }

  /**
   *
   * @returns {Observable<Project>} for UI update handler
   */
  public async sendUpdatedProject(): Promise<Project> {
    // TODO: Indie inject a base service with urls and headers set.
    return await this.http
      .put<Project>(
        this.PUT_PROJECT_URL,
        { project: this.project },
        this.postHeaders
      )
      .toPromise<Project>();
  }

  /**
   * Updates project title
   * @param value
   * @returns {Promise<Observable<Project>>} for UI update handler
   */
  public updateProjectTitle(value: string): Promise<Project> {
    return this.basicAuthService.getToken().then((token: string | null) => {
      this.postHeaders.headers = this.postHeaders.headers.set(
        'Authorization',
        'Bearer ' + token
      );
      this.project.name = value;
      return this.sendUpdatedProject();
    });
  }

  /**
   * Updates project description
   * @param value
   * @returns  {Promise<Observable<Project>>} for UI update handler
   */
  public updateProjectDescription(value: string): Promise<Project> {
    return this.basicAuthService.getToken().then((token: string | null) => {
      this.postHeaders.headers = this.postHeaders.headers.set(
        'Authorization',
        'Bearer ' + token
      );
      this.project.description = value;
      return this.sendUpdatedProject();
    });
  }
}
