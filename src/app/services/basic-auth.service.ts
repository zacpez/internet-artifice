import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebaseApp from 'firebase/compat/app';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Subject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { getApp } from '@angular/fire/app';

@Injectable({
  providedIn: 'root',
})
export class BasicAuthService {
  readonly USER_KEY: string = 'user';
  POST_ONE_TIME_CODE_URL = 'api-user-validateOneTimeCode';
  notification = '';
  unauthorizedAttempt = false;
  user?: firebaseApp.User;

  authEvents: Subject<string> = new Subject<string>();

  constructor(private http: HttpClient, public firebaseAuth: AngularFireAuth) {
    this.POST_ONE_TIME_CODE_URL =
      environment.apiUrl + this.POST_ONE_TIME_CODE_URL;
    this.firebaseAuth.authState.subscribe((user: firebaseApp.User | null) => {
      if (user) {
        this.user = user;
        localStorage.setItem(this.USER_KEY, JSON.stringify(this.user));
      } else {
        delete this.user;
        localStorage.removeItem(this.USER_KEY);
      }
    });
  }

  async getToken(): Promise<string | null> {
    const token = await firebaseApp.auth().currentUser?.getIdTokenResult();
    if (token && new Date(token.expirationTime) > new Date()) {
      return Promise.resolve(token.token);
    }
    const result = await firebaseApp.auth().currentUser?.getIdToken(true);
    return Promise.resolve(result || '');
  }

  login(
    email: string,
    password: string
  ): Promise<firebaseApp.auth.UserCredential> {
    this.unauthorizedAttempt = false;
    return this.firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .catch((data: any) => {
        this.notification = data.message;
        return new Promise(data);
      });
  }

  async register(
    email: string,
    password: string,
    oneTimeCode: string
  ): Promise<firebaseApp.auth.UserCredential> {
    this.unauthorizedAttempt = false;
    const codeValidation = await this.http.post<any>(
      this.POST_ONE_TIME_CODE_URL,
      { email, oneTimeCode }
    );
    console.log(codeValidation);
    return this.firebaseAuth
      .createUserWithEmailAndPassword(email, password)
      .catch((data: any) => {
        this.notification = data.message;
        return new Promise(data);
      });
  }

  logout(): Promise<void> {
    localStorage.removeItem(this.USER_KEY);
    return this.firebaseAuth.signOut();
  }

  loginWithGoogle(): Promise<firebaseApp.auth.UserCredential> {
    const app = getApp();
    return this.firebaseAuth.signInWithPopup(
      new firebaseApp.auth.GoogleAuthProvider()
    );
  }

  /**
   * When an action(click, submit, etc) is performed
   * @todo implement background user authorization, cancel UI/UX if not valid.
   */
  action(notification: string): boolean {
    // Local verification
    this.unauthorizedAttempt = !this.user;
    if (this.unauthorizedAttempt) {
      this.authEvents.next('open');
    }

    // Set specified message
    if (notification) {
      this.notification = notification;
    }

    // Background full validation
    return this.unauthorizedAttempt;
  }
}
