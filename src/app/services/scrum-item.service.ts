import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ScrumItem } from '../models/view/scrum-item';
import { environment } from '../../environments/environment';
import { BasicAuthService } from './basic-auth.service';
import { ProjectService } from './project.service';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ScrumItemService {
  // BLL function names
  ADD_ITEM_URL = 'api-items-addItem';
  UPDATE_ITEM_URL = 'api-items-updateItem';
  DELETE_ITEM_URL = 'api-items-deleteItem';
  GET_ITEMS_URL = 'api-items-getItems';

  // Request headers for this session
  postHeaders: { [headers: string]: HttpHeaders };
  getHeaders: { [headers: string]: HttpHeaders };

  projectName: string = '';

  /**
   *
   * @param http client used to call endpoint
   * @param basicAuthService to track authentication & authorization
   */
  constructor(
    private http: HttpClient,
    private projectService: ProjectService,
    private basicAuthService: BasicAuthService
  ) {
    this.ADD_ITEM_URL = environment.apiUrl + this.ADD_ITEM_URL;
    this.UPDATE_ITEM_URL = environment.apiUrl + this.UPDATE_ITEM_URL;
    this.DELETE_ITEM_URL = environment.apiUrl + this.DELETE_ITEM_URL;
    this.GET_ITEMS_URL = environment.apiUrl + this.GET_ITEMS_URL;

    // Default headers
    const basicHeaders: any = {
      'Content-Type': 'application/json',
    };

    // Store headers for this session's requests
    this.postHeaders = {
      headers: new HttpHeaders(basicHeaders),
    };
    this.getHeaders = {
      headers: new HttpHeaders(basicHeaders),
    };
  }

  // Local copy of scrum items
  items: ScrumItem[] = new Array<ScrumItem>();
  loading = true;
  error?: HttpErrorResponse | null;
  errorEvents: Subject<string> = new Subject<string>();

  /**
   * Save a new item to a project
   * @param item to add
   */
  public addItem(item: ScrumItem): Promise<ScrumItem> {
    return this.basicAuthService
      .getToken()
      .then(async (token: string | null) => {
        this.postHeaders.headers = this.postHeaders.headers.set(
          'Authorization',
          'Bearer ' + token
        );
        return await this.http
          .post<ScrumItem>(
            this.ADD_ITEM_URL,
            { item, project: this.projectService.project.id },
            this.postHeaders
          )
          .toPromise<ScrumItem>();
      });
  }

  /**
   * Update an item in a project
   * @param item to edit
   */
  public updateItem(item: ScrumItem): Promise<ScrumItem> {
    return this.basicAuthService
      .getToken()
      .then(async (token: string | null) => {
        this.postHeaders.headers = this.postHeaders.headers.set(
          'Authorization',
          'Bearer ' + token
        );
        return await this.http
          .post<ScrumItem>(
            this.UPDATE_ITEM_URL,
            { item, project: this.projectService.project.id },
            this.postHeaders
          )
          .toPromise<ScrumItem>();
      });
  }

  /**
   * Delete an item from a project
   * @param item to delete
   */
  public deleteItem(item: ScrumItem): Promise<ScrumItem> {
    return this.basicAuthService
      .getToken()
      .then(async (token: string | null) => {
        this.postHeaders.headers = this.postHeaders.headers.set(
          'Authorization',
          'Bearer ' + token
        );
        return await this.http
          .post<ScrumItem>(
            this.DELETE_ITEM_URL,
            { item, project: this.projectService.project.id },
            this.postHeaders
          )
          .toPromise<ScrumItem>();
      });
  }

  /**
   * Get items from a project
   * @return an array of scrum items
   */
  public getItems(projectName: string): Observable<ScrumItem[]> {
    return this.http.get<ScrumItem[]>(
      this.GET_ITEMS_URL + '?project=' + projectName,
      this.getHeaders
    );
  }

  /**
   * Fetches and stores items in this service from a project
   */
  public loadScrumItems(projectName: string = ''): void {
    this.loading = true;
    this.error = null;
    if (projectName) {
      this.projectName = projectName;
    }
    this.getItems(!projectName ? this.projectName : projectName).subscribe(
      (items: ScrumItem[]) => {
        this.items = this.applyScrumItemsSchema(items);
        this.loading = false;
      },
      (error: HttpErrorResponse) => {
        this.error = error;
        this.errorEvents.next('open');
      }
    );
  }

  /**
   * Apply project item type to  incoming objects
   * @param {any[]} items
   * @returns {ScrumItem[]}
   */
  private applyScrumItemsSchema(items: any[]) {
    return items.map(item => new ScrumItem(item));
  }

  /**
   * @param index of chosen project item
   */
  public removeScrumItem(index: number) {
    this.items = this.items.filter((_, idx) => idx !== index);
  }
}
