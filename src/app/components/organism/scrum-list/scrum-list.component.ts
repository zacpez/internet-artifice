import { Component } from '@angular/core';
import { Category } from 'src/app/models/data/category';
import { ScrumItem } from 'src/app/models/view/scrum-item';
import { ScrumItemService } from 'src/app/services/scrum-item.service';
import {
  CdkDragDrop,
  moveItemInArray,
  CdkDropList,
} from '@angular/cdk/drag-drop';

@Component({
  selector: 'scrum-list',
  templateUrl: './scrum-list.component.html',
  styleUrls: ['./scrum-list.component.scss'],
})
export class ScrumListComponent {
  items: ScrumItem[] = [];

  // @todo: remove static columns
  categoryList: Array<Category> = [
    { title: 'To Do', status: 'todo' },
    { title: 'In Progress', status: 'progress' },
    { title: 'Done', status: 'done' },
  ];

  constructor(public scrumService: ScrumItemService) {}

  /**
   * Styles the number of columns being used
   */
  columnsClass(): string {
    return 'columns--' + this.categoryList.length;
  }

  /**
   * Drag & Drop handler for changing columns, and reordering project items
   * @param event
   * @todo figure out CdkDragDrop types
   */
  public dropHandler(event: CdkDragDrop<any, any>) {
    const newStatus = event.container.data.status;
    moveItemInArray(this.items, event.previousIndex, event.currentIndex);
    const newOrder = this.reorder(event, newStatus);

    // Update the DB
    this.scrumService
      .updateItem({
        ...event.item.data,
        status: newStatus,
        order: newOrder,
      } as ScrumItem)
      .then(() => {
        this.scrumService.loadScrumItems();
      });
  }

  private reorder(event: any, newStatus: string): number {
    const swappedWith = this.scrumService.items
      .filter(item => item.status == newStatus)
      .filter((_, idx) => idx === event.currentIndex)
      .pop();
    if (event.currentIndex > event.previousIndex) {
      return (swappedWith ? swappedWith.order : 0) + 1;
    }
    return (swappedWith ? swappedWith.order : 0) - 1;
  }
}
