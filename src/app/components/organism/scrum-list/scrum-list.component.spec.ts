import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrumListComponent } from './scrum-list.component';

describe('ScrumListComponent', () => {
  let component: ScrumListComponent;
  let fixture: ComponentFixture<ScrumListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ScrumListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
