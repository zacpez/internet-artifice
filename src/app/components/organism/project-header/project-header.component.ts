import { Component, ElementRef, ViewChild } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { BasicAuthService } from 'src/app/services/basic-auth.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'project-header',
  templateUrl: './project-header.component.html',
  styleUrls: ['./project-header.component.scss'],
})
export class ProjectHeaderComponent {
  projectTitleToggle = false;
  projectDescToggle = false;
  formTitle: UntypedFormGroup;
  formDescription: UntypedFormGroup;

  @ViewChild('title', { static: false })
  set title(element: ElementRef<HTMLInputElement>) {
    if (element) {
      element.nativeElement.focus();
    }
  }
  @ViewChild('description', { static: false })
  set description(element: ElementRef<HTMLInputElement>) {
    if (element) {
      element.nativeElement.focus();
    }
  }

  constructor(
    public projectService: ProjectService,
    public basicAuthService: BasicAuthService,
    private readonly formBuilder: UntypedFormBuilder
  ) {
    this.formTitle = this.formBuilder.group({
      title: ['', Validators.required],
    });
    this.formDescription = this.formBuilder.group({
      description: ['', Validators.required],
    });
  }

  editProjectTitleToggle(): void {
    this.projectTitleToggle = !this.projectTitleToggle;
    if (this.projectTitleToggle) {
      const { name: title } = this.projectService.project;
      this.formTitle.patchValue({ title });
      setTimeout(() => {
        this.title.nativeElement.focus();
      }, 100);
    }
  }

  editProjectDescriptionToggle(): void {
    this.projectDescToggle = !this.projectDescToggle;
    if (this.projectDescToggle) {
      const { description } = this.projectService.project;
      this.formDescription.patchValue({ description });
      setTimeout(() => {
        this.description.nativeElement.focus();
      }, 100);
    }
  }

  onClickSubmitTitle() {
    const form = this.formTitle.getRawValue();
    this.projectService.updateProjectTitle(form.title).then(() => {
      this.editProjectTitleToggle();
    });
  }

  onClickSubmitDescription() {
    const form = this.formDescription.getRawValue();
    this.projectService.updateProjectDescription(form.description).then(() => {
      this.editProjectDescriptionToggle();
    });
  }
}
