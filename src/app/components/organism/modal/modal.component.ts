import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  show: boolean = false;

  @Input() eventsSubscription?: Subscription;
  @Input() events?: Observable<any>;
  @Input() canClose: boolean = true;
  @Input() forceShow: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.eventsSubscription = this.events?.subscribe((data: any) => {
      if (data === 'open') {
        this.open();
      } else if (data === 'close') {
        this.close();
      }
    });
  }

  close(): void {
    this.show = false;
  }

  open(): void {
    this.show = true;
  }
}
