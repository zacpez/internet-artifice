import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { BasicAuthService } from 'src/app/services/basic-auth.service';
import { ProjectService } from 'src/app/services/project.service';
import { ScrumItemService } from 'src/app/services/scrum-item.service';

@Component({
  selector: 'modal-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {
  @Input() eventsSubscription?: Subscription;
  @Input() events?: Observable<any>;

  modalEvents: Subject<string> = new Subject<string>();

  constructor(
    public scrumService: ScrumItemService,
    public projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.scrumService.errorEvents?.subscribe((data: string) => {
      if (data === 'open') {
        this.modalEvents.next('open');
      } else if (data === 'submit') {
        this.modalEvents.next('close');
      }
    });
    this.projectService.errorEvents?.subscribe((data: string) => {
      if (data === 'open') {
        this.modalEvents.next('open');
      } else if (data === 'submit') {
        this.modalEvents.next('close');
      }
    });
  }

  public goBack() {
    window.history.back();
  }
}
