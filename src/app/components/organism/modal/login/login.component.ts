import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { BasicAuthService } from 'src/app/services/basic-auth.service';

@Component({
  selector: 'modal-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @Input() eventsSubscription?: Subscription;
  @Input() events?: Observable<any>;

  modalEvents: Subject<string> = new Subject<string>();

  constructor(public basicAuth: BasicAuthService) {}

  ngOnInit(): void {
    this.eventsSubscription = this.events?.subscribe((data: string) => {
      if (data === 'open') {
        this.modalEvents.next('open');
      } else if (data === 'submit') {
        this.modalEvents.next('close');
      }
    });
  }

  onClickSubmit(): void {
    this.modalEvents.next('submit');
  }

  onSuccess(): void {
    this.modalEvents.next('close');
  }
}
