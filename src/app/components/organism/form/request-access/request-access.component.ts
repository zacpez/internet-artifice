import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { BasicAuthService } from 'src/app/services/basic-auth.service';

@Component({
  selector: 'form-request-access',
  templateUrl: './request-access.component.html',
  styleUrls: ['./request-access.component.scss'],
})
export class RequestAccessComponent implements OnInit, OnDestroy {
  formGroup: UntypedFormGroup;

  @Input() eventsSubscription?: Subscription;
  @Input() events?: Observable<any>;
  @Output() successEvent = new EventEmitter<any>();
  @Output() errorEvent = new EventEmitter<any>();

  constructor(
    public basicAuthService: BasicAuthService,
    private readonly formBuilder: UntypedFormBuilder
  ) {
    this.formGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      oneTimeCode: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.eventsSubscription = this.events?.subscribe((data: string) => {
      if (data === 'submit') {
        this.onClickSubmit();
      }
    });
  }

  ngOnDestroy(): void {
    this.eventsSubscription?.unsubscribe();
  }

  onClickSubmit(): void {
    const email: string = this.formGroup.get('email')?.value || '';
    const password: string = this.formGroup.get('password')?.value || '';
    const oneTimeCode: string =
      this.formGroup.get('one-time-code')?.value || '';
    this.basicAuthService
      .register(email, password, oneTimeCode)
      .then(data => this.successEvent.emit(data))
      .catch(error => this.errorEvent.emit(error));
  }
}
