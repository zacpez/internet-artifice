import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnDestroy,
} from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { BasicAuth } from 'src/app/models/view/basic-auth';
import { BasicAuthService } from 'src/app/services/basic-auth.service';

@Component({
  selector: 'form-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  formGroup: UntypedFormGroup;

  @Input() eventsSubscription?: Subscription;
  @Input() events?: Observable<any>;
  @Output() successEvent = new EventEmitter<any>();
  @Output() errorEvent = new EventEmitter<any>();

  constructor(
    public basicAuthService: BasicAuthService,
    private readonly formBuilder: UntypedFormBuilder
  ) {
    this.formGroup = this.formBuilder.group(BasicAuth.getLoginFormGroup());
  }

  ngOnInit(): void {
    this.eventsSubscription = this.events?.subscribe((data: string) => {
      if (data === 'submit') {
        this.onClickSubmit();
      }
    });
  }

  ngOnDestroy(): void {
    this.eventsSubscription?.unsubscribe();
  }

  onClickSubmit(): void {
    const email: string = this.formGroup.get('email')?.value || '';
    const password: string = this.formGroup.get('password')?.value || '';
    this.basicAuthService
      .login(email, password)
      .then(data => this.successEvent.emit(data))
      .catch(error => this.errorEvent.emit(error));
  }
}
