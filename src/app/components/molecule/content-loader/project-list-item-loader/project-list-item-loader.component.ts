import { Component } from '@angular/core';

@Component({
  selector: 'project-list-item-loader',
  templateUrl: './project-list-item-loader.component.html',
  styleUrls: ['./project-list-item-loader.component.scss'],
})
export class ProjectListItemLoaderComponent {
  constructor() {}
}
