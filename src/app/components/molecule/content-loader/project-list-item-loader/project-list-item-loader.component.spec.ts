import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectListItemLoaderComponent } from './project-list-item-loader.component';

describe('ProjectListItemLoaderComponent', () => {
  let component: ProjectListItemLoaderComponent;
  let fixture: ComponentFixture<ProjectListItemLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectListItemLoaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectListItemLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
