import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrumItemLoaderComponent } from './scrum-item-loader.component';

describe('ScrumItemLoaderComponent', () => {
  let component: ScrumItemLoaderComponent;
  let fixture: ComponentFixture<ScrumItemLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ScrumItemLoaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrumItemLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
