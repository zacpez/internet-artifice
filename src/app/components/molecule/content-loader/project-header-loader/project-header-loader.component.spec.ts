import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectHeaderLoaderComponent } from './project-header-loader.component';

describe('ProjectHeaderLoaderComponent', () => {
  let component: ProjectHeaderLoaderComponent;
  let fixture: ComponentFixture<ProjectHeaderLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectHeaderLoaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectHeaderLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
