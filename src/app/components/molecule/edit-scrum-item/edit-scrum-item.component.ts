import { Component, Input, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  NgForm,
  Validators,
} from '@angular/forms';
import Color from 'color';
import { ScrumItem } from 'src/app/models/view/scrum-item';
import { BasicAuthService } from 'src/app/services/basic-auth.service';
import { ScrumItemService } from 'src/app/services/scrum-item.service';

@Component({
  selector: 'edit-scrum-item',
  templateUrl: './edit-scrum-item.component.html',
  styleUrls: ['./edit-scrum-item.component.scss'],
})
/**
 * @todo Move status constants to a data external source
 * @todo refactor form out of this component
 */
export class EditScrumItemComponent implements OnInit {
  @Input() item?: ScrumItem;
  public itemFormGroup: UntypedFormGroup;
  public editing = false;
  colour?: Color;
  backgroundColour?: Color;
  lightColour?: Color;
  darkColour?: Color;

  constructor(
    public scrumService: ScrumItemService,
    public basicAuthService: BasicAuthService,
    private readonly formBuilder: UntypedFormBuilder
  ) {
    this.itemFormGroup = this.formBuilder.group(ScrumItem.getFormGroup());
  }

  ngOnInit(): void {
    if (this.item) {
      this.itemFormGroup.setValue(this.item);
      this.itemFormGroup.get('status')?.setValue(this.item.status);
      this.itemFormGroup.get('colour')?.setValue(this.item.colour);

      this.backgroundColour = new Color(this.item.colour);
      this.colour = new Color(
        this.backgroundColour.isDark() ? '#ffffffcc' : '#000000cc'
      );
      this.darkColour = new Color(this.item.colour).darken(0.9).desaturate(0);
      this.lightColour = new Color(this.item.colour)
        .darken(0.5)
        .desaturate(0.5);
    }
  }

  onClickSubmit(): void {
    this.editing = true;
    this.item = new ScrumItem(this.itemFormGroup.getRawValue());
    this.item.editing = false;
    if (this.item.id) {
      this.scrumService.updateItem(this.item).then(() => {
        this.editing = false;
        this.scrumService.loadScrumItems();
      });
      return;
    }
    this.scrumService.addItem(this.item).then(() => {
      this.scrumService.loadScrumItems();
    });
  }

  onCancelEdit(): void {
    if (this.item) {
      const index = this.scrumService.items.indexOf(this.item);
      if (index !== -1) {
        this.scrumService.items[index].editing = false;
        // If canceling a new item, remove it from the cached list
        if (!this.scrumService.items[index].id) {
          this.scrumService.removeScrumItem(index);
        }
      }
    }
  }
}
