import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditScrumItemComponent } from './edit-scrum-item.component';

describe('EditScrumItemComponent', () => {
  let component: EditScrumItemComponent;
  let fixture: ComponentFixture<EditScrumItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditScrumItemComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditScrumItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
