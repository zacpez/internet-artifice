import { Component, OnInit, Input } from '@angular/core';
import { ScrumItem } from 'src/app/models/view/scrum-item';
import { ToggleState } from 'src/app/models/view/toggle-state';
import { BasicAuthService } from 'src/app/services/basic-auth.service';
import { ScrumItemService } from 'src/app/services/scrum-item.service';
import Color from 'color';

@Component({
  selector: 'scrum-item',
  templateUrl: './scrum-item.component.html',
  styleUrls: ['./scrum-item.component.scss'],
})
export class ScrumItemComponent implements OnInit {
  /**
   * Constants
   * @todo Make a constants provider
   */
  MAX_SHORT = 80;

  // External properties
  @Input() item: ScrumItem = new ScrumItem();

  // Component variables
  startDescription: string = '';
  remainderDescription: string = '';
  toggleTitle: string = '';
  stateDescription: boolean = false;
  colour?: Color;
  backgroundColour?: Color;
  lightColour?: Color;
  darkColour?: Color;

  /**
   * Toggle state for item visual expansion
   * @todo Make a schema or model for toggle state
   */
  toggleStates: ToggleState[] = [
    { title: 'Show more', remainderDescription: '' },
    { title: 'Collapse', remainderDescription: '' },
  ];

  constructor(
    public scrumService: ScrumItemService,
    public basicAuthService: BasicAuthService
  ) {}

  ngOnInit(): void {
    // Prepare description concatenation
    this.startDescription = this.item.description.slice(0, this.MAX_SHORT);
    this.toggleStates[1].remainderDescription = this.item.description.slice(
      this.MAX_SHORT
    );
    this.toggleTitle = this.toggleStates[this.toggleStateIndex()].title;
    this.backgroundColour = new Color(this.item.colour);
    this.colour = new Color(
      this.backgroundColour.isDark() ? '#ffffffcc' : '#000000cc'
    );
    this.darkColour = new Color(this.item.colour).darken(0.9).desaturate(0);
    this.lightColour = new Color(this.item.colour).darken(0.5).desaturate(0.5);
  }

  /**
   * @return an index to a ToggleState for the Description
   */
  toggleStateIndex(): number {
    return this.stateDescription ? 1 : 0;
  }

  /**
   * Perform a state toggle for the Description
   */
  toggleDescription(): void {
    // Change state indicator
    this.stateDescription = !this.stateDescription;

    // Update view model to match state
    const state = this.toggleStates[this.toggleStateIndex()];
    this.remainderDescription = state.remainderDescription;
    this.toggleTitle = state.title;
  }

  editItem(event: MouseEvent): void {
    // Can the user perform the action?
    if (this.basicAuthService.action('You must be logged in to edit items.')) {
      event.preventDefault();
      return;
    }
    const index = this.scrumService.items.indexOf(this.item);
    if (index !== -1) {
      this.scrumService.items[index].editing = true;
    }
  }

  deleteItem(event: MouseEvent): void {
    // Can the user perform the action?
    if (
      this.basicAuthService.action('You must be logged in to delete items.')
    ) {
      event.preventDefault();
      return;
    }
    const WARNING = 'Confirm you are deleting ' + this.item.title;
    if (confirm(WARNING)) {
      this.scrumService.deleteItem(this.item).then(() => {
        this.scrumService.loadScrumItems();
      });
    }
  }
}
