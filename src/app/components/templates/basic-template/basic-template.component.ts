import { ActivatedRoute, Data } from '@angular/router';
import { Component } from '@angular/core';
import { BasicAuthService } from 'src/app/services/basic-auth.service';

@Component({
  selector: 'basic-template',
  templateUrl: './basic-template.component.html',
  styleUrls: ['./basic-template.component.scss'],
})
export class BasicTemplateComponent {
  year: number = 0;
  version: string = '';

  constructor(
    private route: ActivatedRoute,
    public basicAuthService: BasicAuthService
  ) {
    this.route.data.subscribe((data: Data) => {
      this.year = data.year;
      this.version = data.version;
    });
  }
}
