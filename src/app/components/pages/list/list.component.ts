import { ScrumItemService } from './../../../services/scrum-item.service';
import { ScrumItem } from './../../../models/view/scrum-item';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  listName: string = '';
  modalEvents: Subject<string> = new Subject<string>();

  constructor(
    private route: ActivatedRoute,
    public scrumService: ScrumItemService
  ) {
    this.route.paramMap.subscribe((data: any) => {
      this.listName = data.params.listName;
      this.refreshListService(this.listName);
    });
  }

  insertDummyCard(): void {
    const WARNING = 'Cancel previous scrum item change?';
    const foundIndex = this.scrumService.items.findIndex(
      (item: ScrumItem) => item.editing
    );
    if (foundIndex !== -1) {
      // No, we want to remain editing last item
      if (!confirm(WARNING)) {
        return;
      }
      // Yes, we want to edit the next item
      this.scrumService.items[foundIndex].editing = false;
    }

    // Setup new item to edit
    const newItem = new ScrumItem();
    newItem.editing = true;
    newItem.status = 'todo';
    this.scrumService.items.unshift(newItem);
  }

  addListItem(): void {
    this.insertDummyCard();
  }

  refreshListService(projectName: string): void {
    this.scrumService.loadScrumItems(projectName);
  }
}
