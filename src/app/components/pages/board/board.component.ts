import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { ScrumItem } from 'src/app/models/view/scrum-item';
import { ScrumItemService } from 'src/app/services/scrum-item.service';
import { ProjectService } from 'src/app/services/project.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'page-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
})
export class BoardPageComponent implements OnInit {
  projectName: string = '';
  modalEvents: Subject<string> = new Subject<string>();

  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    public scrumService: ScrumItemService,
    public projectService: ProjectService
  ) {
    this.route.paramMap.subscribe((data: any) => {
      this.projectName = data.params.projectName;
    });
  }

  ngOnInit() {
    this.refreshScrumService();
  }

  insertDummyCard(): void {
    const WARNING = 'Cancel previous scrum item change?';
    const foundIndex = this.scrumService.items.findIndex(
      (item: ScrumItem) => item.editing
    );
    if (foundIndex !== -1) {
      // No, we want to remain editing last item
      if (!confirm(WARNING)) {
        return;
      }
      // Yes, we want to edit the next item
      this.scrumService.items[foundIndex].editing = false;
    }

    // Setup new item to edit
    const newItem = new ScrumItem();
    newItem.editing = true;
    newItem.status = 'todo';
    this.scrumService.items.unshift(newItem);
  }

  addItemCard(): void {
    this.insertDummyCard();
  }

  refreshScrumService(): void {
    this.scrumService.loadScrumItems(this.projectName);
    this.projectService.loadProjectInfo(this.projectName).subscribe(() => {
      this.titleService.setTitle(
        `${this.projectService.project.name} - Internet Artifice`
      );
    });
  }
}
