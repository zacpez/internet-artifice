import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Project } from 'src/app/models/data/project';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'search-boards-page',
  templateUrl: './search-boards-page.component.html',
  styleUrls: ['./search-boards-page.component.scss'],
})
export class SearchBoardsPageComponent implements OnInit {
  queryAsString = 'Projects';
  projects: Project[] = [];
  isProjectsLoading = true;

  constructor(
    private route: ActivatedRoute,
    public projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.projectService.getProjects().subscribe(data => {
      this.projects = data;
      this.isProjectsLoading = false;
    });
  }
}
