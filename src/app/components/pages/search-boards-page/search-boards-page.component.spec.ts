import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBoardsPageComponent } from './search-boards-page.component';

describe('SearchBoardsPageComponent', () => {
  let component: SearchBoardsPageComponent;
  let fixture: ComponentFixture<SearchBoardsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchBoardsPageComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoardsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
